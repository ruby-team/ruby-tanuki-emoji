# frozen_string_literal: true

require "bundler/gem_tasks"
require "rspec/core/rake_task"
require "rubocop/rake_task"
require "tanuki_emoji"

RSpec::Core::RakeTask.new(:spec) do |t|
  t.rspec_opts = %w[--format documentation --color --require spec_helper]
  t.rspec_opts += %w[--format RspecJunitFormatter --out rspec.xml] if ENV['GITLAB_CI']
end

RuboCop::RakeTask.new

desc 'Code coverage detail'
task :simplecov do
  ENV['COVERAGE'] = 'true'
  Rake::Task['spec'].execute
end

task default: %i[spec rubocop]

desc 'Update images from vendor/noto-emoji to app/assets folder'
task update_assets: ['noto_emoji:generate_flags'] do
  png_path = File.join(__dir__, 'vendor/noto-emoji/png/72')
  flag_path = File.join(__dir__, 'vendor/noto-emoji/build/flags')
  TanukiEmoji.index.all.each do |emoji|
    if emoji.flag?
      source_flag = File.join(flag_path, emoji.image_name)
      cp(source_flag, TanukiEmoji.images_path)

      next
    end

    source_png = File.join(png_path, emoji.image_name)
    cp(source_png, TanukiEmoji.images_path)
  end
end

namespace :noto_emoji do
  noto_emoji_path = File.join(__dir__, 'vendor/noto-emoji')

  task :generate_flags do
    chdir(File.expand_path(noto_emoji_path)) do
      flags = Dir['third_party/region-flags/png/*.png'].map do |file|
        File.join('build/flags', File.basename(file))
      end

      sh('make', *flags)
    end
  end
end

Rake::Task[:build].enhance [:update_assets]
